<h1><center><strong>IntelliJ使用指导</strong></center></h1>

[官方Features介绍](https://www.jetbrains.com/idea/features/)

[主题可以在线安装](https://plugins.jetbrains.com/search?isPaid=false&tags=Theme)

# 1、常用快捷键

- `duble Shift`全局搜索，不仅仅是搜索文件，还包括action、符号等
- `shift command O`搜索文件
- `shift command A`搜索actions
- `alter(option) enter`鼠标放到错误提示的地方，按该快捷键查看错误提示及修复。如果没有错误也会有优化代码的建议
- `F2`跳转到下一个错误的地方
- `command 1`目录结构和编辑窗口之间切换。
- `ESC`从其他任何位置跳转到编辑窗口
- `command E`查看最近打开的文件列表
- `command B`跳转到定义的地方
- `ctrl command B`跳转到实现的地方
- `alter f7`查看所有使用的地方
- `double ctrl`运行任何程序
- `option ⬆️ / option ⬇️`来选择代码块
- `command /`快速添加注释
- `shift ctrl /`快速添加多行注释
- `option command L `格式化代码
- `shift option command L`修改格式化规则
- `ctrl T`代码重构

# 2、插件

- 可以通过`*Settings / Plugins*.`查看所有的插件
- 图表，可以查看类之间的关系
- 可以自动重构代码
- 模板可以让你快速的插入预先定义好的代码段

