use std::string::String;

fn main()
{
    let mut s = String::with_capacity(100);
    s.push_str("123");
    println!("{}, {}, {}", s, s.len(), s.capacity());

    let s_from_vec = String::from_utf8(vec![0xe4, 0xbd, 0xa0, 0xe5, 0xa5, 0xbd]);
    if s_from_vec.is_err() {
        println!("Error");
        return;
    }
    println!("{}", s_from_vec.unwrap());

    let invalid_utf8 = vec![0xff, 0xff, 0xff];

    let s_from_invalid_utf8 = String::from_utf8_lossy(&invalid_utf8);
    println!("{}", s_from_invalid_utf8);

    let s_from_utf16 = String::from_utf16(&[0x4F60, 0x597D]);
    println!("{}", s_from_utf16.unwrap());

    let tmp_s = String::from("hello");
    let s_str = tmp_s.as_str();
    print!("{}", s_str);

    let mut s_origin = String::with_capacity(10);
    s_origin.push('1');
    s_origin.reserve(10);
    println!("{}", s_origin.capacity());

}