fn main()
{
    println!("hello chengwen");
    let res = Solution::roman_to_int("LVIII".to_string());
    println!("{}", res)
}

struct Solution{}

impl Solution {
    pub fn roman_to_int(s: String) -> i32 {
        // 如果当前字符大于或者等于后一个字符，那么加上当前字符的值,继续下一个字符
        let mut res:i32 = 0;
        let mut chars_iter = s.chars().peekable();
        while let Some(curr_char) = chars_iter.next() {
            let  next_char = chars_iter.peek();
            if next_char != None {
                if Self::compare(curr_char, *next_char.unwrap()) >= 0 {
                    res += Self::get_val(curr_char);
                } else {
                    res += Self::get_val(*next_char.unwrap()) - Self::get_val(curr_char);
                    chars_iter.next();
                }
            } else {
                res += Self::get_val(curr_char);
            }
        }
        // 如果当前字符小于下一个值，那么用下一个值减去当前值，并累加结果。跳过下一个字符。
        //
        return res;
    }

    fn compare(left:char, right:char) -> i32 {
        let base_chars = "IVXLCDM".to_string();
        let mut left_index:i32 = 0;
        let mut right_index = 0;
        for (i, item) in base_chars.chars().enumerate() {
            if left == item {
                left_index = i as i32;
            } else if right == item {
                right_index = i as i32;
            }
        }
        return (left_index - right_index) as i32;
    }

    fn get_val(roman:char) -> i32 {
        if roman == 'I' {
            return 1;
        } else if roman == 'V' {
            return 5;
        }  else if roman == 'X' {
            return 10;
        } else if roman == 'L' {
            return 50;
        } else if roman == 'C' {
            return 100;
        } else if roman == 'D' {
            return 500;
        } else if roman == 'M' {
            return 1000;
        }
        return 0;
    }
}