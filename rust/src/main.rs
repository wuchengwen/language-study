fn main() {
    println!("Hello, world! {}", another_function(10));
    let animal = Animal::new(2, "Lion");
    animal.say_hello();
}

// define after main
fn another_function(i:i32) -> i32 { // snake case style
    println!("Another function.i={i}, i={}", i); // print variable
    i + 1   // expression as return value
}

struct Animal {
    age:u32,
    name:&'static str,
}

impl Animal {
    // associated function
    fn new(age:u32, name:&'static str) -> Self {
        Animal { age, name }    // expression as return value
    }

    // method
    fn say_hello(&self) {
        println!("Hello, my name is {} and I am {} years old.", self.name, self.age);
    }
}

