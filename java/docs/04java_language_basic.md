[toc]

# 3、在你的程序中创建数组

- 和c++的数组一致，大小不可变。

- 使用方法：

  ```java
  // declares an array of integers
  int[] anArray;
  
  // allocates memory for 10 integers
  anArray = new int[10];
  ```

## 声明一个变量去引用一个数组

- string也可以有数组

  ```java
  String[] anArrayOfStrings;
  ```

- []可以放在类型名后面，也可以放在变量名后面，建议是放在类型名后面，因为他表示数组类型。

## 创建、初始化和访问一个数组

- 在创建的同时初始化一个数组

  ```java
  int[] anArray = { 
      100, 200, 300,
      400, 500, 600, 
      700, 800, 900, 1000
  };
  
  ```

- 多维数组

  - 在java中，多维数组是元素是数组类型的数组。所以说数组的行，长度可以不一样。如下：

    ```java
    class MultiDimArrayDemo {
        public static void main(String[] args) {
            String[][] names = {
                {"Mr. ", "Mrs. ", "Ms. "},
                {"Smith", "Jones"}
            };
            // Mr. Smith
            System.out.println(names[0][0] + names[1][0]);
            // Ms. Jones
            System.out.println(names[0][2] + names[1][1]);
        }
    }
    
    ```

  - 通过数组的length方法可以知道其长度。

## 拷贝数组

