[toc]

# 一、GetStarted

- 如果是单个.java文件的java应用，可以直接使用`java`命令一键式编译和运行该java应用
- java的类名必须和文件名一致，这是强制要求的。
- java官方学习网站：https://dev.java/learn/

## 准备编译环境

- JDK(java development kit)、JRE(java runtime enviroment)、OpenJDK、J2EE(java Enterprise     Edition)之间的关系

- - JDK是由OpenJDK和Oracle官方发布的java开发工具，JRE是java运行时环境，是JDK里面的一部分，且JRE不在由官方发布。我们不应该使用JRE的工具去编译代码。J2EE故名思义，就是Oracle公司发布的商用的java版本。
  - Java SE是java标准版

- JDK长期支持的官方网站：https://jdk.java.net/

## 编译代码

- `javac HelloWorld.java`     将java文件编译为.class文件 。
- `java HelloWorld`运行刚编译的java程序。

## 总结

java程序的开发步骤：

1. 编写`.java`文件。
2. 将这些`.java`文件编译成`.class`二进制文件。
3. 将这些`.class`文件放一块作为java应用来运行。

# 二、Launching Single-File Source-Code Programs

## 单文件源码程序执行

- 从JDK 11 开始，java支持不需要预先编译成.class而可以直接通过`java`拉起单个源文件的java程序，在运行时会将编译结果暂存在内存中。
- 这对于学习java新特性非常方便。
- 但同时这个特性的使用也有一些限制。

```java
class HelloWorld {
    public static void main(String[] arg) {
        System.out.println("Hello World!");
    }
}

/* 执行命令：java HelloWorld.java */
```



## 单个源文件中的多个类

```java
public class MultipleClass {
    public static void main(String[] args){
        System.out.println(GenerateMsg.GetMsg());
        System.out.println(AnotherMsg.GetMsg());
    }
}

public class GenerateMsg {
    static String GetMsg() {
        return "Generate Msg";
    }
}

public class AnotherMsg {
    static String GetMsg() {
        return "Another Msg";
    }
}
```

## 在类Unix系统中按照脚本执行java代码

```java
#!/usr/bin/java --source 16
class HelloWorld {
    public static void main(String[] arg) {
        System.out.println("Hello World!");
    }
}

/* 执行命令：./HelloWorld */
```

