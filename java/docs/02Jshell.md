# 一、介绍jshell

- `jshell`一个交互式，java编程语言，方便学习新特性

- `jshell [options] [load-files]`,options在下面有介绍，load-files可以包含多个文件，文件内容可以是java代码或者命令

- 示例：`jshell --show-version jshellScript`

  `jshellScript`文件内容如下：

  ```java
  /help intro
  ```

 执行结果：

<img src="assets/image-20211124214703851.png" alt="image-20211124214703851" style="zoom: 50%;" />

- [load-files]也可以是预先定义的如下符号：

  DEFAULT：

  JAVASE：导入所有java标准定义的包

  PRINTING:

- 命令行的脚本，是在启动脚本之后运行，在jshell运行的时候，可以使用命令`/open`打开脚本

- 要接受来自标准输入的输入并抑制交互式I/O，请为load-file输入连字符(-)。此选项允许在管道链中使用jshell工具。

- jshell启动之后，通过输入`/list -start`来查看启动脚本，`/exit`退出jshell

- 可以通过向上的箭头，选取已经输入的变量或者函数，然后修改他们。

其他的不怎么常用，用到的时候可以在学习https://dev.java/learn/jshell-tool/