[toc]

# 0 、相关概念

- interface是一组方法的集合，实现了所有这些方法，就实现了这个接口
- package是一个包，将关系比较紧密的类放一块，组成一个包，代表一个模块
- java SE API:https://docs.oracle.com/en/java/javase/17/docs/api/index.html

# 1、创建变量和命名

## 变量的分类

- **实例变量（非静态变量）**：这种变量，每个实例有**不同**的值。
- **类变量（静态变量）**：这种变量，所有该类的实例都有**相同**的值。并且可以将这种变量使用关键字**final**来修饰，以表示后续不能在修改。
- **局部变量**：有效范围在声明变量的局部有效
- **参数**：函数参数，只能称作是变量，不能成为field

## 变量命名

- 变量名是大小写敏感的，不限长度，有字母、数字、下划线，$符号，@符号组成。且一般以字母开头。
- 变量名使用小驼峰，常量采用全大写加下划线格式

# 2、在程序中创建原始类型变量

## 原始类型

- java语言，是静态类型的，也就是说，所有变量在使用之前，必须先声明。
- java原始数据类型：
  - byte:8位，有符号数（-128～127）
  - short：16位，有符号数
  - int：32位，有符号数。从java SE8 开始，可以使用Integer类来表示无符号整型。
  - long：64位，有符号数，可以使用Long类，来表示无符号数
  - float：单精度，32位，IEEE754标准浮点数，这个类型不能用于精度要求较高的场景，如果要用建议使用[`java.math.BigDecimal`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/math/BigDecimal.html)类。
  - double：双精度64位浮点，同样不能用于高精度场景，
  - boolean：虽然只有一位信息，但是大小没有精确的定义
  - Char：是一个16位的字符。
- 除了上述类型，java语言对于string类型也有特殊的支持。
  - string不是原始类型，但你可以认为他是原始类型。**string是不可变的**

## 使用默认值来初始化一个变量

- 如果变量在声明时没有初始化，会被赋予一个默认值。但这不是一个好的习惯。

- 原始类型，默认初始值：

  | Data Type              | Default Value (for fields) |
  | ---------------------- | -------------------------- |
  | byte                   | 0                          |
  | short                  | 0                          |
  | int                    | 0                          |
  | long                   | 0L                         |
  | float                  | 0.0f                       |
  | double                 | 0.0d                       |
  | char                   | `\u0000`                   |
  | String (or any object) | null                       |
  | boolean                | `false`                    |

- 编译器不会对局部变量赋予初始值，如果局部变量不初始化直接使用，则会导致运行时错误。

  ```java
  // 测试代码
  void TestLocal()
  {
    int a;
    println(a);
  }
  ```

## 使用字面量创建变量

- 原始类型不像其他类型，不是对象类型，不需要使用new，可以直接使用字面量赋值，如下：

```java
boolean result = true;
char capitalC = 'C';
byte b = 100;
short s = 10000;
int i = 100000;
```

- 整型字面量

  - 如果一个字面量以L或者l结尾，则表示long类型，否则表示int类型。建议使用大写L.
  - Byte, short,int和long类型可以用int字面量来创建。
  - 字面量`0x`开头，表示16进制，`0b`开头表示二进制。

  ```java
  // The number 26, in decimal
  int decimalValue = 26;
  
  //  The number 26, in hexadecimal
  int hexadecimalValue = 0x1a;
  
  // The number 26, in binary
  int binaryValue = 0b11010;
  
  ```

- 浮点字面量

  - 如果一个浮点以F或者f结尾，则表明是float类型，否则是double类型。当然double类型也可以以d或者D结尾。

  - 浮点也可以用E或者e来表示，科学记数法表示。

    ```java
    double d1 = 123.4;
    
    // same value as d1, but in scientific notation
    double d2 = 1.234e2;
    float f1  = 123.4f;
    
    ```

- 字符和字符串字面常量

  - 可以使用转意符+Unicode编码，来表示字符和字符串：
    - 列如：`\u0108`表示大写字母C。
    - `\b` (backspace), `\t` (tab), `\n` (line feed), `\f` (form feed), `\r` (carriage return), `\"` (double quote), `\'` (single quote), and `\\` (backslash).
    - `null`可以赋值给任何非原始类型，表示当前变量无效。
    - 类可以通过类名+.class来表示类的字面量

- 在数字字面常量中使用下划线

  - 可以使用下划线作为数字的分隔符，方便阅读如下：

    ```java
    long creditCardNumber = 1234_5678_9012_3456L;
    long socialSecurityNumber = 999_99_9999L;
    float pi =  3.14_15F;
    long hexBytes = 0xFF_EC_DE_5E;
    long hexWords = 0xCAFE_BABE;
    long maxLong = 0x7fff_ffff_ffff_ffffL;
    byte nybbles = 0b0010_0101;
    long bytes = 0b11010010_01101001_10010100_10010010;
    
    ```

  - 注意如下位置不能使用：

    - At the beginning or end of a number
    - Adjacent to a decimal point in a floating point literal
    - Prior to an `F` or `L` suffix
    - In positions where a string of digits is expected