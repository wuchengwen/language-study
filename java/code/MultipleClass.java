public class MultipleClass {
    public static void main(String[] args){
        System.out.println(GenerateMsg.GetMsg());
        System.out.println(AnotherMsg.GetMsg());
    }
}

public class GenerateMsg {
    static String GetMsg() {
        return "Generate Msg";
    }
}

public class AnotherMsg {
    static String GetMsg() {
        return "Another Msg";
    }
}