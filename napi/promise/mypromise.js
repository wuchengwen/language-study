// hello.js
const mypromise = require('./build/Release/mypromise');

mypromise.hiPromise(true)
    .then(() => { console.log("param true then"); })
    .catch(() => { console.log("param true catch"); })

mypromise.hiPromise(false)
    .then(() => { console.log("param false then"); })
    .catch(() => { console.log("param false catch"); })

process.stdin.resume();