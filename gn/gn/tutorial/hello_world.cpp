#include <algorithm>
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

#include <dlfcn.h>

#include "container.h"
using namespace std;

int main()
{
    cout << "Hello World";
    std::chrono::seconds dura(2);

    // 起新线程，加载所有so.
    std::thread td([&dura]()
                   {
        // 为了 解耦更加彻底，可以将该vector替换为从配置文件中读取要加载的so列表。
        std::vector<std::string> sos {"libaddmember.so", "libaddmember1.so"};
        for_each(sos.begin(), sos.end(), [&dura](std::string &soPath) {
            auto ret = dlopen(soPath.c_str(), RTLD_NOW);
            if (ret == nullptr) {
                std::cout << "dlopen failed" << std::endl;
            }
            std::this_thread::sleep_for(dura);
        }); });

    // 打印注册进来的所有成员
    for (int x = 0; x < 5; ++x)
    {
        PrintMembers();
        std::this_thread::sleep_for(dura);
    }
    td.join();
    return 0;
}

// 编译命令
/*
gn gen out/Default
ninja -C out/Default hello_world
out/Default/hello_world

*/

// 调试时可以通过ps查看进程号
// 然后通过pmap -x pid查看本进程加载的所有so.