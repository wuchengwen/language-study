#include "container.h"

#include <iostream>

__attribute__((constructor)) static void Construct(void);
// 在该so加载完成后，即调用构造函数，完成本模块的自动注册。
void Construct(void)
{
    AddMember(22);
}