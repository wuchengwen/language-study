#include "container.h"

#include <algorithm>
#include <iostream>
#include <vector>
std::vector<int> g_mems;

void AddMember(int a)
{
    g_mems.emplace_back(a);
}

void PrintMembers()
{
    std::cout << "members: ";
    for_each(g_mems.begin(), g_mems.end(), [](int a) {
        std::cout << ' ' << a;
    });
    std::cout << std::endl;
}