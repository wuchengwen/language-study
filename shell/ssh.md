<h1><strong><center>ssh配置及用法</center></strong></h1>

# 1、ubuntu配置远程ssh连接

1. `apt install ssh`安装ssh
2. `sudo vim /etc/ssh/ssh_config `编辑ssh配置，去掉`#  Port 22`以及`#  PasswordAuthentication yes`前面的注释符号
3. `ssh start`启动ssh服务
4. `service ssh status`查看ssh服务是否正常启动
5. 客户端登录`ssh username@hostname`
   - 注意这里的username不能是重命名之后的名字，必须是原始的用户名

