<h1><strong><center>Linux常用命令</center></strong></h1>

[linux教程](https://www.linux.org/forums/linux-beginner-tutorials.123/)

# 1、chmod

- 首先我们使用命令`ls -l`查看当前目录下的文件：
- 

# 2、解决普通用户无法登陆问题

- 有时候，我们手动修改了相关内容，导致普通用户登不进去了，此时可以按照如下方法操作。
- 首先重启，并`长按shift键`，进入开机画面。
- 选择列表中第一个`recovery-mode`，回车。
- 选择`root登录`，此时已root身份进入系统
- 此时许多文件无法写入，需要执行如下命令`mount -o rw,remount /`
- 在root下进行相关操作

# 3、df

- `df -lh`查看linux磁盘使用情况

# 4、查看内存信息

- `cat /proc/meminfo | grep MemTotal`

# 5、挂载磁盘

[参考链接](https://blog.csdn.net/make_progress/article/details/118571375)

- root下使用命令 `fdisk -l`查找新添加的盘符
- 
