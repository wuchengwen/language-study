#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int main()
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("Failed to create socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(12345);
    inet_pton(AF_INET, "192.168.101.5", &server_addr.sin_addr);

    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("Failed to connect to server");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    char msg[4*1024*1024] = {0};
    memset(msg, 'a', sizeof(msg));
    if (send(sockfd, msg, sizeof(msg), 0) < 0)
    {
        perror("Failed to send message");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    // char buffer[256] = {0};
    // ssize_t bytes_received = recv(sockfd, buffer, sizeof(buffer) - 1, 0);
    // if (bytes_received <= 0)
    // {
    //     if (bytes_received == 0)
    //     {
    //         printf("Server disconnected\n");
    //     }
    //     else
    //     {
    //         perror("Failed to receive data from server");
    //     }
    // }
    // else
    // {
    //     buffer[bytes_received] = '\0';
    //     printf("Received: %s\n", buffer);
    // }
    sleep(1);

    close(sockfd);
    return 0;
}