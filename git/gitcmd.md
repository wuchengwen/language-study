<h1><center><strong>git常用命令</strong></center></h1>

[git官方网站](https://git-scm.com/)

[廖雪峰的git教程](https://www.liaoxuefeng.com/wiki/896043488029600)

# 1、git防止每次输用户名密码

- `git remote -v`如果显示如下:

  ```shell
  origin	https://gitee.com/wuchengwen/language-study.git (fetch)
  origin	https://gitee.com/wuchengwen/language-study.git (push)
  ```

  

  则证明是https通信，这样每次都需要输入用户名、密码。我们将其修改为ssh通信就可以避免这个问题。

- 删除原有的origin通信：`git remote rm origin`

- 添加ssh公钥：

  - `ssh-keygen`按照提示生成公钥。
  - `cat ~/.ssh/id_rsa.pub`查看生成的公钥。
  - 将公钥添加到服务端账户中

- 重新添加ssh的origin:`git remote add origin git@gitee.com:wuchengwen/language-study.git`

- 设置分支：`git branch --set-upstream-to=origin/master master`

- `git pull`验证配置

# 2、查看git配置

- 查看仓库级的 config，命令：`git config --local -l`
- 查看全局级的 config，命令：`git config --global -l`
- 查看系统级的 config，命令：`git config--system -l`
- 查看当前生效的配置，  命令：`git config -l`

# 3、repo删除本地所有修改

`repo forall -c 'git checkout . && git clean -xdf'`

